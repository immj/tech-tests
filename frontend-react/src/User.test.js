import { render, screen } from '@testing-library/react';
import User from './User';

const userData = {
    id: {
        value: "1"
    },
    name: {
        first: "First",
        last: "Last",
    },
    picture: {
        medium: "avatar.jpg"
    }
}

describe("User", () => {
    test('renders user name', () => {
      render(<User user={userData}/>);
    
      const username = screen.getByTestId("username");
      
      expect(username).toBeInTheDocument();
      expect(username).toHaveTextContent("First Last");
    });
    
    test('renders user avatar', () => {
      render(<User user={userData}/>);
    
      expect(screen.getByAltText('avatar')).toHaveAttribute("src", "avatar.jpg");
    });
    
    //add at least one more test
});
