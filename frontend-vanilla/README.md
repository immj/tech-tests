# Frontend Vanilla JS Test

## Description

We've had request to add a feature to the frontend. We need to be able to 
generate a notification once the user has clicked subscribe.



## Acceptance Criteria


1. Use the browser [Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_A) to generate
notifications.
   
2. If subscribe button is clicked and permission is granted, a notification is generated.

3. If subscribe button is clicked and permission is not granted, no notifications appear.

4. The notification message should say ```"User has subscribed"```

## Working with the project

| command              | description                                                                          |
| -------------------- | ------------------------------------------------------------------------------------ |
| `npm start`          | Starts a dev server at [http://localhost:8080](http://localhost:8080) (includes live reloading)         |
