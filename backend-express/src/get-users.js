const fetch = require("node-fetch"); // https://www.npmjs.com/package/node-fetch

async function getUsers (req, res) {
    const result = await fetch("https://randomuser.me/api?results=10&nat=gb"); // https://randomuser.me

    // Process the result of the above fetch...
    //
    // We need to return an array of "users" in the following shape...
    // ```
    // {
    //      "title": "<TITLE>",
    //      "name": "<FIRST NAME> <LAST NAME>",
    //      "age": <AGE (AS NUMBER)>,
    //      "email": "<EMAIL>",
    //      "avatar": "<THUMBNAIL PICTURE>"
    // }
    // ```
    // 
    // The response JSON should be in the following shape...
    // ```
    // {
    //      "status": <RESPONSE STATUS CODE (AS NUMBER)>,
    //      "users": [
    //             { ... <USER OBJECT AS ABOVE> },
    //             ...
    //       ]
    // }
    // ```
    // 
    // If we have trouble loading the data bfrom the fetch, we need to return a JSON response in the
    // following shape...
    // ```
    // {
    //      "status": <RESPONSE STATUS CODE (AS NUMBER)>,
    //      "error": "<ERROR MESSAGE>"
    // }
    // ```

    return res.json({
        users:[
            {
                title: "ms",
                name: "test user",
                age: 30,
                email: "test.user@email.com",
                avatar: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
            },
        ],
    });
}

module.exports = getUsers;
