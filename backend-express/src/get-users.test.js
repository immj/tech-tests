const test = require("ava"); // https://www.npmjs.com/package/ava
const sinon = require("sinon"); // https://www.npmjs.com/package/sinon
const proxyquire = require("proxyquire").noCallThru().noPreserveCache(); // https://www.npmjs.com/package/proxyquire

test.beforeEach((t) => {
    t.context.req = {};

    t.context.res = {
        json: sinon.stub(),
    };

    t.context.getUsers = proxyquire("./get-users", { "node-fetch": sinon.stub() });
});

test("should return an array of users in the JSON response", async (t) => {
    await t.context.getUsers(t.context.req, t.context.res);

    t.true(t.context.res.json.calledOnce);

    const [json] = t.context.res.json.getCall(0).args;

    t.true(Array.isArray(json.users));
    t.true(typeof json.users[0] === "object");
    t.true(typeof json.users[0].title === "string");
    t.true(typeof json.users[0].name === "string");
    t.true(typeof json.users[0].age === "number");
    t.true(typeof json.users[0].email === "string");
    t.true(typeof json.users[0].avatar === "string");
});

// Add at least one more test case below...
