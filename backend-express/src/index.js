const express = require("express"); // https://www.npmjs.com/package/express
const getUsers = require("./get-users");

const app = express();

app.get("/api/users", getUsers);

app.listen(3000, () => {
    console.log("Server listening at http://localhost:3000");
});
